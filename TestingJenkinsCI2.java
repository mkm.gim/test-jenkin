/** 
 * The TestingJenkinsCI2 class implements an application that simply prints "Hello World" to standard output. jen test
 */
public class TestingJenkinsCI2 {
  public static void main(String[] args) {
    System.out.println("Hello, World");
    System.out.println("Test pass");
  }
}
